import {ChambreDTO} from "./chambreDTO";
import {Chambre} from "../../domain/chambre";
import {Etage, EtageDeux, EtageTrois, EtageUn, RezDeChaussee} from "../../domain/etage";
import {Prix} from "../../domain/prix";

export const chambreDtoToChambre = (chambreDto: ChambreDTO) : Chambre => {
    return new Chambre(numberToEtage(chambreDto.etage), {numero: chambreDto.numero}, new Prix(chambreDto.prix))
}

export const chambreToChambreDto = (chambre: Chambre) : ChambreDTO => {
    return {
        etage: chambre.etage.numero,
        numero:  chambre.identifiant.numero,
        prix: chambre.prix.valeur
    }
}

const numberToEtage = (etageNumber: number): Etage => {
    switch (etageNumber) {
        case 0:
            return new RezDeChaussee()
        case 1:
            return new EtageUn()
        case 2:
            return new EtageDeux()
        case 3:
            return new EtageTrois()
        default:
            throw new Error("Numéro d'étage inconnu")
    }
}
