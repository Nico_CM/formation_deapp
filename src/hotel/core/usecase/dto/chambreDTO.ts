export interface ChambreDTO {
    readonly etage: number
    readonly numero: number
    readonly prix: number
}
