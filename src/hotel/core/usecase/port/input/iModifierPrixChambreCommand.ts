export interface IModifierPrixChambreCommand {
    execute(prixRezDeChaussee: number): void;
}
