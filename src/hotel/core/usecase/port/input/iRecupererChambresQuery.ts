import {IChambrePresenteur} from "../output/iChambrePresenteur";

export interface IRecupererChambresQuery {
    execute(chambrePresenteur: IChambrePresenteur): void;
}
