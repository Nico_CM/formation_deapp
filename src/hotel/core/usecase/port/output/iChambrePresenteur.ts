import {ChambreDTO} from "../../dto/chambreDTO";

export interface IChambrePresenteur {
    listeChambres: ChambreDTO[];

    present(chambres: ChambreDTO[]): void;
}
