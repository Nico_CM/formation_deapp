import {IHotelRepository} from "./gateway/IHotelRepository";
import {IModifierPrixChambreCommand} from "./port/input/iModifierPrixChambreCommand";

export class ModifierPrixChambreCommand implements IModifierPrixChambreCommand {
    constructor(private hotelRepository: IHotelRepository) {
    }

    execute(prixRezDeChaussee: number) {
        const hotel = this.hotelRepository.recupererHotel();
        hotel.mettreAJourPrixChambres(prixRezDeChaussee)
        this.hotelRepository.modifierHotel(hotel)
    }
}
