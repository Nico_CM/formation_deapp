import {IHotelRepository} from "./gateway/IHotelRepository";
import {IRecupererChambresQuery} from "./port/input/iRecupererChambresQuery";
import {chambreToChambreDto} from "./dto/chambreMapper";
import {IChambrePresenteur} from "./port/output/iChambrePresenteur";

export class RecupererChambresQuery implements IRecupererChambresQuery {
    constructor(private hotelRepository: IHotelRepository) {
    }

    execute(chambrePresenteur: IChambrePresenteur): void {
        const chambres = this.hotelRepository.recupererHotel().listerChambres().map(chambre => chambreToChambreDto(chambre));
        chambrePresenteur.present(chambres)
    }
}
