import { Hotel } from "../../domain/hotel";

export interface IHotelRepository {
    recupererHotel(): Hotel;
    modifierHotel(hotel: Hotel): void;
}
