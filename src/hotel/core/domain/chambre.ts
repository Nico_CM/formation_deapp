import {Etage} from "./etage";
import {Prix} from "./prix";

interface IdentifiantChambre {
   numero : number
}

export class Chambre {
    constructor(public readonly etage: Etage, public readonly identifiant: IdentifiantChambre, public prix: Prix) {}

    modifierPrix(nouveauPrixRezDeChaussee: number): void {
         this.prix = Prix.calculerNouvelleValeur(nouveauPrixRezDeChaussee, this.etage.tauxParRapportRezDeChaussee)
    }
}
