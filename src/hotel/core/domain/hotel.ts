import {Chambre} from "./chambre";

export class Hotel {
    constructor(private chambres: Chambre[]) {
    }

    public listerChambres(): readonly Chambre[] {
        return this.chambres
    }

    public mettreAJourPrixChambres(nouveauPrixRezDeChaussee: number) {
        this.chambres.forEach(chambre => {
                chambre.modifierPrix(nouveauPrixRezDeChaussee)
            }
        )
    }

}
