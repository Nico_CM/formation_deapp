const VALEUR_MAXIMUM = 200;

export class Prix {
    constructor(public readonly valeur: number) {

    }

    static calculerNouvelleValeur(nouveauPrixRezDeChaussee: number, pourcentageSupplementaire: number): Prix {
        return new Prix(Math.min(nouveauPrixRezDeChaussee * pourcentageSupplementaire, VALEUR_MAXIMUM))
    }
}
