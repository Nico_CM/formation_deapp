export interface Etage {
    tauxParRapportRezDeChaussee: number
    numero: number
}

export class RezDeChaussee implements Etage {
    tauxParRapportRezDeChaussee = 1
    numero = 0
}

export class EtageUn implements Etage {
    tauxParRapportRezDeChaussee = 1.07
    numero = 1
}

export class EtageDeux implements Etage {
    tauxParRapportRezDeChaussee = 1.22
    numero = 2
}

export class EtageTrois implements Etage {
    tauxParRapportRezDeChaussee = 1.33
    numero = 3
}
