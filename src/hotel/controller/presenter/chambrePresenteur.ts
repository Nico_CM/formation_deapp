import {IChambrePresenteur} from "../../core/usecase/port/output/iChambrePresenteur";
import {ChambreDTO} from "../../core/usecase/dto/chambreDTO";

export class ChambrePresenteur implements IChambrePresenteur {
    public listeChambres: ChambreDTO[] = [];

    present(chambres: ChambreDTO[]): void {
        this.listeChambres = chambres
    }
}
