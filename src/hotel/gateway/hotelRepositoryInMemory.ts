import {IHotelRepository} from "../core/usecase/gateway/IHotelRepository";
import {Hotel} from "../core/domain/hotel";

export class HotelRepositoryInMemory implements IHotelRepository {
    constructor(private hotel: Hotel) {
    }

    recupererHotel(): Hotel {
        return this.hotel;
    }

    modifierHotel(hotel: Hotel): void {
        this.hotel = hotel
    }

}
