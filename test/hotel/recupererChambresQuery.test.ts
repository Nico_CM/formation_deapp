import { it, expect, describe } from "@jest/globals"
import {HotelRepositoryInMemory} from "../../src/hotel/gateway/hotelRepositoryInMemory";
import { RecupererChambresQuery } from "../../src/hotel/core/usecase/recupererChambresQuery";
import {ChambreDTO} from "../../src/hotel/core/usecase/dto/chambreDTO";
import {Chambre} from "../../src/hotel/core/domain/chambre";
import {RezDeChaussee} from "../../src/hotel/core/domain/etage";
import {ChambrePresenteur} from "../../src/hotel/controller/presenter/chambrePresenteur";
import {Hotel} from "../../src/hotel/core/domain/hotel";
import {Prix} from "../../src/hotel/core/domain/prix";


describe("Récupération des chambres", () => {
    it("Si l'hotel ne possède aucune chambre alors ne recupère aucune chambre", () => {
        const hotel: Hotel = new Hotel([]);
        const hotelRepositoryInMemory = new HotelRepositoryInMemory(hotel)
        const chambrePresenteur: ChambrePresenteur = new ChambrePresenteur()

        const recupererChambresQuery = new RecupererChambresQuery(hotelRepositoryInMemory)

        const chambresAttendues: ChambreDTO[] = []

        recupererChambresQuery.execute(chambrePresenteur)
        expect(chambrePresenteur.listeChambres).toStrictEqual(chambresAttendues)
    })

    it("Si l'hotel possède 2 chambres alors recupère les 2 chambres", () => {
        const hotel: Hotel = new Hotel([
            new Chambre(new RezDeChaussee(), {numero: 1}, new Prix(50)),
            new Chambre(new RezDeChaussee(), {numero: 2}, new Prix(50))
        ]);
        const chambreRepositoryInMemory = new HotelRepositoryInMemory(hotel)
        const chambrePresenteur: ChambrePresenteur = new ChambrePresenteur()

        const recupererChambresQuery = new RecupererChambresQuery(chambreRepositoryInMemory)

        const chambresAttendues: ChambreDTO[] = [
            {"etage": 0, "numero": 1, "prix": 50},
            {"etage": 0, "numero": 2, "prix": 50}
        ];

        recupererChambresQuery.execute(chambrePresenteur)
        expect(chambrePresenteur.listeChambres).toStrictEqual(chambresAttendues)
    })
})
