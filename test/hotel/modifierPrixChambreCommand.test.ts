import { it, expect, describe } from "@jest/globals"
import {Chambre} from "../../src/hotel/core/domain/chambre";
import {HotelRepositoryInMemory} from "../../src/hotel/gateway/hotelRepositoryInMemory";
import {RezDeChaussee, EtageUn, EtageDeux, EtageTrois} from "../../src/hotel/core/domain/etage";
import {ModifierPrixChambreCommand} from "../../src/hotel/core/usecase/modifierPrixChambreCommand";
import { Hotel } from "../../src/hotel/core/domain/hotel";
import { Prix } from "../../src/hotel/core/domain/prix";

describe("Mise à jour des prix", () => {
    it("ETANT DONNÉ que le prix des chambres du rdc est à 50 euros" +
        "QUAND le prix des chambres du rdc est mis à jour" +
        "ALORS le nouveau prix est récupéré", () => {
        // ETANT DONNÉ
        const HOTEL_ACTUEL = new Hotel([new Chambre(new RezDeChaussee(), {numero: 1}, new Prix(50)), new Chambre(new RezDeChaussee(), {numero: 2}, new Prix(50))])
        const HOTEL_ATTENDU = new Hotel([new Chambre(new RezDeChaussee(), {numero: 1}, new Prix(70)), new Chambre(new RezDeChaussee(), {numero: 2}, new Prix(70))])
        const hotelRepositoryInMemory = new HotelRepositoryInMemory(HOTEL_ACTUEL)

        const recuperationChambreService = new ModifierPrixChambreCommand(hotelRepositoryInMemory)

        // QUAND
        recuperationChambreService.execute(70)

        // ALORS
        expect(hotelRepositoryInMemory.recupererHotel()).toStrictEqual(HOTEL_ATTENDU)
    })

    it("ETANT DONNÉ que le prix des chambres du rdc est à 50 euros" +
        "QUAND le prix des chambres du rdc est mis à jour" +
        "ALORS les chambres des 3 étages au dessus sont également mises à jour avec le même prix augmenté de 7%, 22% et 33%", () => {
        // ETANT DONNÉ
        const HOTEL_ACTUEL = new Hotel([
            new Chambre(new RezDeChaussee(), {numero: 1}, new Prix(50)),
            new Chambre(new EtageUn(), {numero: 2}, new Prix(50)),
            new Chambre(new EtageDeux(), {numero: 3}, new Prix(50)),
            new Chambre(new EtageTrois(), {numero: 4}, new Prix(50)),
        ]);
        const HOTEL_ATTENDU = new Hotel([
            new Chambre(new RezDeChaussee(), {numero: 1}, new Prix(70)),
            new Chambre(new EtageUn(), {numero: 2}, new Prix(70 * 1.07)),
            new Chambre(new EtageDeux(), {numero: 3}, new Prix(70 * 1.22)),
            new Chambre(new EtageTrois(), {numero: 4}, new Prix(70 * 1.33)),
        ]);
        const chambreRepositoryInMemory = new HotelRepositoryInMemory(HOTEL_ACTUEL)

        const recuperationChambreService = new ModifierPrixChambreCommand(chambreRepositoryInMemory)

        // QUAND
        recuperationChambreService.execute(70)

        // ALORS
        expect(chambreRepositoryInMemory.recupererHotel()).toStrictEqual(HOTEL_ATTENDU)
    })

    it("ETANT DONNÉ que le prix des chambres du rdc est à 50 euros" +
        "QUAND le prix des chambres du rdc est mis à jour à 199 euros" +
        "ALORS les chambres des 3 étages au dessus sont également mises à jour mais plafonnées à 200 euros", () => {
        // ETANT DONNÉ
        const HOTEL_ACTUEL = new Hotel([
            new Chambre(new RezDeChaussee(), {numero: 1}, new Prix(50)),
            new Chambre(new EtageUn(), {numero: 2}, new Prix(50)),
            new Chambre(new EtageDeux(), {numero: 3}, new Prix(50)),
            new Chambre(new EtageTrois(), {numero: 4}, new Prix(50)),
        ]);
        const HOTEL_ATTENDU = new Hotel([
            new Chambre(new RezDeChaussee(), {numero: 1}, new Prix(199)),
            new Chambre(new EtageUn(), {numero: 2}, new Prix(200)),
            new Chambre(new EtageDeux(), {numero: 3}, new Prix(200)),
            new Chambre(new EtageTrois(), {numero: 4}, new Prix(200)),
        ]);
        const chambreRepositoryInMemory = new HotelRepositoryInMemory(HOTEL_ACTUEL)

        const recuperationChambreService = new ModifierPrixChambreCommand(chambreRepositoryInMemory)

        // QUAND
        recuperationChambreService.execute(199)

        // ALORS
        expect(chambreRepositoryInMemory.recupererHotel()).toStrictEqual(HOTEL_ATTENDU)
    })
})
